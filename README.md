# Teste Android Evino

Desenvolvido na linguagem Kotlin, o app consome a api do Github e lista os gists na primeira activity e mostra os detalhes do gist selecionado na segunda activity. Adicionei a funcionalidade de mostrar no navegador do usuário o conteúdo dos arquivos do gist, após este ser selecionado na lista de arquivos na tela de detalhamento.

### **Funcionalidades** ###

- Teste de conexão com a internet nas duas activities.
- Paginação na tela de lista na primeira activity, com endless scroll / scroll infinito
- SwipeRefreshLayout(arraste a tela para baixo para atualizar o conteúdo) nas duas activities.
- Animação na transição das Activities nas duas activities.
- Cache após o request dos gists.
- Arquivos de config do proguard.

### **Tratamento de rotção de tela** ###

Para demosntrar que existem várias formas de tratar a rotação de tela eu usei duas estratégias. A primeira activity eu informo no Manifest que quero que ela só funcione em modo portrait(em pé) e na segunda activity eu guardo os dados no Bundle para evitar requests desnecessários.

### **Bibliotecas** ###

Retrofit, Rxandroid, Dagger, Picasso, Espresso, JUnit, Okhttp