package com.testeAndroidEvino.data

import com.testeAndroidEvino.App
import com.testeAndroidEvino.data.remote.APIService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideGistRepository(api: APIService, app: App): GistsDataSource = GistsRepository(api, app)

    @Provides
    @Singleton
    fun provideGistDetailRepository(api: APIService, app: App): GistDetailDataSource = GistDetailRepository(api, app)
}
