package com.testeAndroidEvino.data

import com.testeAndroidEvino.App
import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.data.model.GistDetail
import com.testeAndroidEvino.data.remote.APIService
import rx.Observable

class GistsRepository(private val api: APIService, private val app: App) : GistsDataSource {
    override fun getGists(page: Int): Observable<List<Gist>> = api.getGists(page)
}

class GistDetailRepository(private val api: APIService, private val app: App) : GistDetailDataSource {
    override fun getGistDetail(id: String): Observable<GistDetail> = api.getGistDetail(id)
}
