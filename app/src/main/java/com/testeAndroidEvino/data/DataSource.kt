package com.testeAndroidEvino.data

import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.data.model.GistDetail
import rx.Observable

interface GistsDataSource {
    fun getGists(page: Int): Observable<List<Gist>>
}

interface GistDetailDataSource {
    fun getGistDetail(id: String): Observable<GistDetail>
}
