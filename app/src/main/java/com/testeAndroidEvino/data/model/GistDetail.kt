package com.testeAndroidEvino.data.model

import android.os.Parcel
import android.os.Parcelable

data class GistDetail(val description: String?, val owner: Owner?, val files: Map<String, File>?) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readParcelable<Owner>(Owner::class.java.classLoader),

            mapOf<String, File>().apply {
                source.readMap(this, File::class.java.classLoader)
            }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(description)
        writeParcelable(owner, 0)
        writeMap(files)
        writeParcelableMap(dest, files, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<GistDetail> = object : Parcelable.Creator<GistDetail> {
            override fun createFromParcel(source: Parcel): GistDetail = GistDetail(source)
            override fun newArray(size: Int): Array<GistDetail?> = arrayOfNulls(size)
        }
    }

    private fun <T : Parcelable> writeParcelableMap(dest: Parcel, objects: Map<String, T>?, flags: Int) {
        if (objects == null) {
            dest.writeInt(-1)
        } else {
            dest.writeInt(objects.keys.size)
            for (key in objects.keys) {
                dest.writeString(key)
                dest.writeParcelable(objects[key], flags)
            }
        }
    }
}
