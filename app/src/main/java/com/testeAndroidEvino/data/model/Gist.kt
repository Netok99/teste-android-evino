package com.testeAndroidEvino.data.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Gist(private val url: String, val owner: Owner?, val id: String, private val description: String?,
                val created_at: Date, val files: Map<String, File>?) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readParcelable<Owner>(Owner::class.java.classLoader),
            source.readString(),
            source.readString(),
            source.readSerializable() as Date,
            mapOf<String, File>().apply {
                source.readMap(this, File::class.java.classLoader)
            }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(url)
        writeParcelable(owner, 0)
        writeString(id)
        writeString(description)
        writeSerializable(created_at)
        writeMap(files)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Gist> = object : Parcelable.Creator<Gist> {
            override fun createFromParcel(source: Parcel): Gist = Gist(source)
            override fun newArray(size: Int): Array<Gist?> = arrayOfNulls(size)
        }
    }
}
