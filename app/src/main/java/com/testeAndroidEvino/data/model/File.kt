package com.testeAndroidEvino.data.model

import android.os.Parcel
import android.os.Parcelable

data class File(val filename: String, val size: Int, val type: String, val language: String?,
                val raw_url: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(filename)
        writeInt(size)
        writeString(type)
        writeString(language)
        writeString(raw_url)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<File> = object : Parcelable.Creator<File> {
            override fun createFromParcel(source: Parcel): File = File(source)
            override fun newArray(size: Int): Array<File?> = arrayOfNulls(size)
        }
    }
}
