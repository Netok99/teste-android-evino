package com.testeAndroidEvino.data.remote

import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.data.model.GistDetail
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

interface APIService {

    @GET("gists")
    fun getGists(@Query("page") page: Int): Observable<List<Gist>>

    @GET("/gists/{id}")
    fun getGistDetail(@Path("id") id: String): Observable<GistDetail>
}
