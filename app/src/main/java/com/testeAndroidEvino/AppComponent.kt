package com.testeAndroidEvino

import android.content.Context
import com.testeAndroidEvino.data.APIModule
import com.testeAndroidEvino.data.DataModule
import com.testeAndroidEvino.presentation.PresenterModule
import com.testeAndroidEvino.presentation.UiComponent
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, DataModule::class, APIModule::class))
interface AppComponent {
    fun inject(app: App)
    operator fun plus(presenterModule: PresenterModule): UiComponent
}

@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideApp(): App = app
}
