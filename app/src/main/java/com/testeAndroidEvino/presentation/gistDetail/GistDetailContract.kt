package com.testeAndroidEvino.presentation.gistDetail

import com.testeAndroidEvino.data.model.GistDetail
import com.testeAndroidEvino.presentation.BaseView

interface GistDetailContract {
    interface View : BaseView {
        fun setupViews(gistDetail: GistDetail)

        fun showError()
    }

    interface Presenter {
        fun getGistDetail(id: String)
    }
}
