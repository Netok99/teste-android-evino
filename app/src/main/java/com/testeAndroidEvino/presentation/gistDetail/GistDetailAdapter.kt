package com.testeAndroidEvino.presentation.gistDetail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.testeAndroidEvino.R
import com.testeAndroidEvino.data.model.File
import com.testeAndroidEvino.data.model.GistDetail
import kotlinx.android.synthetic.main.item_gist_detail.view.*

class GistDetailAdapter(private val context: Context, private val gistDetail: GistDetail):
        RecyclerView.Adapter<GistDetailViewHolder>() {

    override fun onBindViewHolder(holder: GistDetailViewHolder, position: Int) =
            holder.bind(context, gistDetail.files!!.toList()[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GistDetailViewHolder =
            GistDetailViewHolder(LayoutInflater.from(context).inflate(R.layout.item_gist_detail, parent, false))

    override fun getItemCount(): Int {
        if (gistDetail.files != null) {
            return gistDetail.files.size
        }
        return 0
    }
}

class GistDetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(context: Context, pair: Pair<String, File>) = with(itemView) {
        val file = pair.second
        txtFileName.text = file.filename
        txtSize.text = context.getString(R.string.size, file.size.toString())
        txtType.text = file.type

        if (file.language.isNullOrEmpty()) {
            txtLanguage.visibility = View.GONE
        } else {
            txtLanguage.visibility = View.VISIBLE
            txtLanguage.text = file.language
        }

        setOnClickListener {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(file.raw_url)))
        }
    }
}
