package com.testeAndroidEvino.presentation.gistDetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.testeAndroidEvino.R
import com.testeAndroidEvino.data.model.GistDetail
import com.testeAndroidEvino.presentation.*
import kotlinx.android.synthetic.main.activity_gist_detail.*
import javax.inject.Inject

class GistDetailActivity : BaseActivity(), GistDetailContract.View {

    private val keyId = "id"
    private val keyGistDetail = "gistDetail"
    private lateinit var uiComponent: UiComponent
    private var gistDetail: GistDetail? = null
    @Inject
    lateinit var presenter: GistDetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gist_detail)
        uiComponent = getAppComponent() + PresenterModule(this)
        uiComponent.inject(this)

        setupUI()

        if (savedInstanceState == null) {
            getGistDetail()
        } else {
            setupViews(savedInstanceState.getParcelable(keyGistDetail))
        }
    }

    private fun setupUI() {
        setupToolbar()
        setupSwipeRefresh()
    }

    private fun getGistDetail() {
        if (isOnline(this)) {
            presenter.getGistDetail(intent.getStringExtra(keyId))
            offline_state.visibility = View.GONE
            content.visibility = View.VISIBLE
        } else {
            swipeRefresh.isRefreshing = false
            offline_state.visibility = View.VISIBLE
            content.visibility = View.GONE
        }
    }

    private fun setupToolbar() {
        toolbar.title = "Detail Gist"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setupSwipeRefresh() = swipeRefresh.setOnRefreshListener({ getGistDetail() })

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        if (gistDetail != null) {
            savedInstanceState.putParcelable(keyGistDetail, gistDetail)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setupViews(gistDetail: GistDetail) {
        this.gistDetail = gistDetail
        setupGistDetailList(gistDetail)

        if (!gistDetail.owner?.avatar_url.isNullOrEmpty()) imgUser.loadUrl(gistDetail.owner?.avatar_url)
        fillTextView(this, txtDescription, gistDetail.description)
        fillTextView(this, txtUsername, gistDetail.owner?.login)
    }

    private fun setupGistDetailList(gistDetail: GistDetail) {
        val layoutManager = LinearLayoutManager(this)
        gistDetailList.layoutManager = layoutManager
        gistDetailList.addItemDecoration(DividerItemDecoration(gistDetailList.context, layoutManager.orientation))
        gistDetailList.adapter = GistDetailAdapter(this, gistDetail)
        gistDetailList.setHasFixedSize(true)
    }

    @SuppressLint("StringFormatMatches")
    override fun showError() = Toast.makeText(this, getString(R.string.error_message_detail_gist),
            Toast.LENGTH_LONG).show()

    override fun showLoadingIndicator(isLoading: Boolean) {
        swipeRefresh.isRefreshing = isLoading
    }
}
