package com.testeAndroidEvino.presentation.gistDetail

import android.util.Log
import com.testeAndroidEvino.data.GistDetailDataSource
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class GistDetailPresenter(private val view: GistDetailContract.View,
                          private val dataSource: GistDetailDataSource) : GistDetailContract.Presenter {

    override fun getGistDetail(id: String) {
        view.showLoadingIndicator(true)

        dataSource.getGistDetail(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate {
                    view.showLoadingIndicator(false)
                }
                .subscribe({ gistDetail ->
                    view.setupViews(gistDetail)
                }, { error ->
                    Log.e("error", error.toString())
                    view.showError()
                })
    }
}
