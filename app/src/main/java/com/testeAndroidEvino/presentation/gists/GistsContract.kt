package com.testeAndroidEvino.presentation.gists

import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.presentation.BaseView

interface GistsContract {
    interface View : BaseView {
        fun setupRecyclerView(gists: List<Gist>)

        fun showError()
    }

    interface Presenter {
        fun getGists(page: Int)
    }
}
