package com.testeAndroidEvino.presentation.gists

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.testeAndroidEvino.R
import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.presentation.fillTextView
import com.testeAndroidEvino.presentation.gistDetail.GistDetailActivity
import com.testeAndroidEvino.presentation.loadUrl
import kotlinx.android.synthetic.main.item_gist.view.*
import java.text.SimpleDateFormat

class GistsAdapter(private val activity: GistsActivity) : RecyclerView.Adapter<GistsViewHolder>() {

    private var gists: MutableList<Gist> = mutableListOf()

    fun addItem(newGists: List<Gist>) {
        for (gist in newGists) {
            gists.add(gist)
            notifyItemInserted(gists.size - 1)
        }
    }

    fun clearItems() {
        val size = gists.size
        gists.clear()
        notifyItemRangeRemoved(0, size)
    }

    override fun onBindViewHolder(holder: GistsViewHolder, position: Int) = holder.bind(activity, gists[position])

    override fun getItemCount(): Int = gists.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GistsViewHolder =
            GistsViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_gist, parent, false))
}

class GistsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @SuppressLint("SimpleDateFormat")
    fun bind(activity: GistsActivity, gist: Gist) = with(itemView) {
        fillTextView(context, txtFileName, gist.files?.values!!.toList()[0].filename)
        fillTextView(context, txtUsername, gist.owner?.login)
        txtCreatedAt.text = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(gist.created_at)

        val quantityFiles = gist.files.size
        val txtFiles = if (quantityFiles > 1) context.getString(R.string.files, quantityFiles.toString())
        else context.getString(R.string.file, quantityFiles.toString())
        fillTextView(context, txtQtdFiles, txtFiles)
        if (!gist.owner?.avatar_url.isNullOrEmpty()) imgUser.loadUrl(gist.owner?.avatar_url)

        setOnClickListener {
            val intent = Intent(context, GistDetailActivity::class.java)
            intent.putExtra("id", gist.id)
            context.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }
}
