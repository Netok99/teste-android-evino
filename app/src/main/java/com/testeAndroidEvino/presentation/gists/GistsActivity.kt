package com.testeAndroidEvino.presentation.gists

import android.annotation.SuppressLint
import android.os.Bundle
import com.testeAndroidEvino.R
import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.presentation.BaseActivity
import com.testeAndroidEvino.presentation.EndlessRecyclerOnScrollListener
import com.testeAndroidEvino.presentation.PresenterModule
import com.testeAndroidEvino.presentation.UiComponent
import kotlinx.android.synthetic.main.activity_gists.*
import javax.inject.Inject
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast

class GistsActivity : BaseActivity(), GistsContract.View {

    private lateinit var uiComponent: UiComponent
    private lateinit var adapter: GistsAdapter
    @Inject
    lateinit var presenter: GistsContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gists)
        uiComponent = getAppComponent() + PresenterModule(this)
        uiComponent.inject(this)

        setupUI()
        getGists(1)
    }

    private fun setupUI() {
        setupToolbar()
        setupSwipeRefresh()
        setupGistsList()
    }

    private fun setupToolbar() {
        toolbar.title = getString(R.string.title_gists_activity)
        setSupportActionBar(toolbar)
    }

    private fun setupSwipeRefresh() {
        swipeRefresh.setOnRefreshListener({
            adapter.clearItems()
            getGists(1)
            setupGistsList()
        })
    }

    private fun setupGistsList() {
        val layoutManager = LinearLayoutManager(this)
        gistsList.layoutManager = layoutManager
        gistsList.addOnScrollListener(
                EndlessRecyclerOnScrollListener(layoutManager, { getGists(it) }))
        gistsList.addItemDecoration(DividerItemDecoration(gistsList.context, layoutManager.orientation))
        adapter = GistsAdapter(this)
        gistsList.adapter = adapter
        gistsList.setHasFixedSize(true)
    }

    private fun getGists(page: Int) {
        if (isOnline(this)) {
            presenter.getGists(page)
            offline_state.visibility = View.GONE
        } else {
            swipeRefresh.isRefreshing = false
            offline_state.visibility = View.VISIBLE
        }
    }

    @SuppressLint("StringFormatMatches")
    override fun showError() = Toast.makeText(this, getString(R.string.error_message_public_gists),
            Toast.LENGTH_LONG).show()

    override fun setupRecyclerView(gists: List<Gist>) = adapter.addItem(gists)

    override fun showLoadingIndicator(isLoading: Boolean) {
        swipeRefresh.isRefreshing = isLoading
    }
}
