package com.testeAndroidEvino.presentation.gists

import android.util.Log
import com.testeAndroidEvino.data.GistsDataSource
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class GistsPresenter(private val view: GistsContract.View,
                     private val dataSource: GistsDataSource) : GistsContract.Presenter {

    override fun getGists(page: Int) {
        view.showLoadingIndicator(true)

        dataSource.getGists(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate {
                    view.showLoadingIndicator(false)
                }
                .subscribe({ gists ->
                    view.setupRecyclerView(gists)
                }, { error ->
                    Log.e("error", error.toString())
                    view.showError()
                })
    }
}
