package com.testeAndroidEvino.presentation

import com.testeAndroidEvino.data.GistDetailDataSource
import com.testeAndroidEvino.data.GistsDataSource
import com.testeAndroidEvino.presentation.gistDetail.GistDetailActivity
import com.testeAndroidEvino.presentation.gistDetail.GistDetailContract
import com.testeAndroidEvino.presentation.gistDetail.GistDetailPresenter
import com.testeAndroidEvino.presentation.gists.GistsActivity
import com.testeAndroidEvino.presentation.gists.GistsContract
import com.testeAndroidEvino.presentation.gists.GistsPresenter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(PresenterModule::class))
interface UiComponent {
    fun inject(activity: GistsActivity)
    fun inject(activity: GistDetailActivity)
}

@Module
open class PresenterModule(private val view: BaseView) {
    @Provides
    fun provideGistsPresenter(dataSource: GistsDataSource): GistsContract.Presenter {
        if (view !is GistsContract.View) {
            throw ClassCastException("view should be GistsContract.View")
        }
        return GistsPresenter(view, dataSource)
    }

    @Provides
    fun provideGistPresenter(dataSource: GistDetailDataSource): GistDetailContract.Presenter {
        if (view !is GistDetailContract.View) {
            throw ClassCastException("view should be GistDetailContract.View")
        }
        return GistDetailPresenter(view, dataSource)
    }
}
