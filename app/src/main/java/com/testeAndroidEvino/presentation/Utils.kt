package com.testeAndroidEvino.presentation

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.testeAndroidEvino.R

fun ImageView.loadUrl(url: String?) = Picasso.with(context).load(url).into(this)

fun fillTextView(context: Context, textView: TextView, text: String?) {
    textView.text = if (text.isNullOrEmpty()) context.getString(R.string.without_text) else text
}
