package com.testeAndroidEvino

import com.testeAndroidEvino.data.GistDetailDataSource
import com.testeAndroidEvino.data.GistsDataSource
import com.testeAndroidEvino.data.model.File
import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.data.model.GistDetail
import com.testeAndroidEvino.data.model.Owner
import com.testeAndroidEvino.presentation.gistDetail.GistDetailContract
import com.testeAndroidEvino.presentation.gistDetail.GistDetailPresenter
import com.testeAndroidEvino.presentation.gists.GistsContract
import com.testeAndroidEvino.presentation.gists.GistsPresenter
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import rx.lang.kotlin.toSingletonObservable
import java.util.*

class GistDetailPresenterTest {

    @Rule
    @JvmField
    val rxField = RxSchedulersOverrideRule()

    private lateinit var view: GistDetailContract.View
    private lateinit var presenter: GistDetailContract.Presenter
    private lateinit var dataSource: GistDetailDataSource

    @Before
    fun setup() {
        view = mock()
        dataSource = mock()
        presenter = GistDetailPresenter(view, dataSource)
    }

    private val file1 = File("teste.kt", 100, "text", "kotlin", "https://test")
    private val file2 = File("teste2.kt", 200, "text", "kotlin", "https://test2")

    private val gistDetail =
            GistDetail("description test", Owner("neto", "https://github.com/neto"),
                    mapOf("test" to file1, "test2" to file2))

    @Test
    fun shouldGetGistList() {
        val id = "sjddakjfh"

        Mockito.`when`(dataSource.getGistDetail(id))
                .thenReturn(gistDetail.toSingletonObservable())

        presenter.getGistDetail(id)

        Mockito.verify(view).showLoadingIndicator(true)
        Mockito.verify(dataSource).getGistDetail(id)
        Mockito.verify(view).setupViews(gistDetail)
        Mockito.verify(view).showLoadingIndicator(false)
        Mockito.verifyNoMoreInteractions(view, dataSource)
    }
}
