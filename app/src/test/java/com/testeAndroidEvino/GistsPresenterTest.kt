package com.testeAndroidEvino

import com.testeAndroidEvino.data.GistsDataSource
import com.testeAndroidEvino.data.model.File
import com.testeAndroidEvino.data.model.Gist
import com.testeAndroidEvino.data.model.Owner
import com.testeAndroidEvino.presentation.gists.GistsContract
import com.testeAndroidEvino.presentation.gists.GistsPresenter
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import rx.lang.kotlin.toSingletonObservable
import java.util.*

class GistsPresenterTest {

    @Rule
    @JvmField
    val rxField = RxSchedulersOverrideRule()

    private lateinit var view: GistsContract.View
    private lateinit var presenter: GistsContract.Presenter
    private lateinit var dataSource: GistsDataSource

    @Before
    fun setup() {
        view = mock()
        dataSource = mock()
        presenter = GistsPresenter(view, dataSource)
    }

    private val file1 = File("teste.kt", 100, "text", "kotlin", "https://test")
    private val file2 = File("teste2.kt", 200, "text", "kotlin", "https://test2")

    private val gistsList = listOf(
            Gist("https://api.github.com/gists/aa5a315d61ae9438b18d",
                    Owner("neto", "https://github.com/neto"), "fdkfldslfns",
                    "description test", Date(), mapOf("test" to file1, "test2" to file2)),
            Gist("https://api.github.com/gists/aa5a315d61ae9438b18d",
                    Owner("joao", "https://github.com/joao"), "jgfjdgn",
                    "description test", Date(), mapOf("test" to file1, "test2" to file2))
    )

    @Test
    fun shouldGetGistList() {
        val page = 1

        Mockito.`when`(dataSource.getGists(page))
                .thenReturn(gistsList.toSingletonObservable())

        presenter.getGists(page)

        Mockito.verify(view).showLoadingIndicator(true)
        Mockito.verify(dataSource).getGists(page)
        Mockito.verify(view).setupRecyclerView(gistsList)
        Mockito.verify(view).showLoadingIndicator(false)
        Mockito.verifyNoMoreInteractions(view, dataSource)
    }
}
