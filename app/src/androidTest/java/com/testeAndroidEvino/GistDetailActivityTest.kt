package com.testeAndroidEvino

import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import com.testeAndroidEvino.presentation.gistDetail.GistDetailActivity
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class GistDetailActivityTest {

    private var server: MockWebServer? = null

    @Rule
    @JvmField
    val mActivityRule= ActivityTestRule(GistDetailActivity::class.java, false, false)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        server = MockWebServer()
        server!!.start()
        setupServerUrl()
    }

    private fun setupServerUrl() {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        server!!.url("https://api.github.com/gists/aa5a315d61ae9438b18d")
    }

    @Test
    fun whenResultIsOkShouldDisplayListWithGists() {
        server!!.enqueue(MockResponse().setResponseCode(200).setBody(Mock.SUCCESS_PULL))
        mActivityRule.launchActivity(createIntent())
        Espresso.onView(ViewMatchers.withId(R.id.gistDetailList)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    private fun createIntent(): Intent {
        return Intent().putExtra("id", "teste")
    }

    @Test
    fun checkUserItemViewIsDisplayed() {
        server!!.enqueue(MockResponse().setResponseCode(200).setBody(Mock.SUCCESS_PULL))
        mActivityRule.launchActivity(createIntent())
        Espresso.onView(Matchers.allOf<View>(ViewMatchers.withId(R.id.txtUsername), ViewMatchers.withText("jimczi"))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        server!!.shutdown()
    }
}
