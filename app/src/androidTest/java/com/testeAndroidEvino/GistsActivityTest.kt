package com.testeAndroidEvino

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import com.testeAndroidEvino.presentation.gistDetail.GistDetailActivity
import com.testeAndroidEvino.presentation.gists.GistsActivity
import com.testeAndroidEvino.presentation.gists.GistsViewHolder
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class GistsActivityTest {

    private var server: MockWebServer? = null

    @Rule
    @JvmField
    val mActivityRule= ActivityTestRule(GistsActivity::class.java, false, false)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        server = MockWebServer()
        server!!.start()
        setupServerUrl()
    }

    private fun setupServerUrl() {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        server!!.url("https://api.github.com/gists")
    }

    @Test
    fun whenResultIsOkShouldDisplayListWithGists() {
        server!!.enqueue(MockResponse().setResponseCode(200))
        mActivityRule.launchActivity(Intent())
        Espresso.onView(ViewMatchers.withId(R.id.gistsList)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    private fun createIntent(): Intent {
        return Intent().putExtra("id", "teste")
    }

    @Test
    fun checkUserItemViewIsDisplayed() {
        server!!.enqueue(MockResponse().setResponseCode(200))
        mActivityRule.launchActivity(createIntent())
        Espresso.onView(Matchers.allOf<View>(ViewMatchers.withId(R.id.txtUsername),
                ViewMatchers.withText("jimczi"))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun whenClickOnItemListShouldStartUserDetailsActivityWithExtra() {
        server!!.enqueue(MockResponse().setResponseCode(200))
        mActivityRule.launchActivity(Intent())
        Intents.init()

        val matcher = CoreMatchers.allOf<Intent>(
                IntentMatchers.hasComponent(GistDetailActivity::class.java.name),
                IntentMatchers.hasExtraWithKey("id")
        )

        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, null)
        Intents.intending(matcher).respondWith(result)
        Espresso.onView(ViewMatchers.withId(R.id.gistsList)).perform(
                RecyclerViewActions.actionOnItemAtPosition<GistsViewHolder>(0, ViewActions.click()))
        Intents.intended(matcher)
        Intents.release()
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        server!!.shutdown()
    }
}
