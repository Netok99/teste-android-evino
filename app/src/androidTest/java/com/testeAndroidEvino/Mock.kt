package com.testeAndroidEvino

internal interface Mock {
    companion object {

        val SUCCESS_PULL = "[\n" +
                "  {\n" +
                "    \"url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/5140\",\n" +
                "    \"id\": 108126574,\n" +
                "    \"html_url\": \"https://github.com/ReactiveX/RxJava/pull/5140\",\n" +
                "    \"diff_url\": \"https://github.com/ReactiveX/RxJava/pull/5140.diff\",\n" +
                "    \"patch_url\": \"https://github.com/ReactiveX/RxJava/pull/5140.patch\",\n" +
                "    \"issue_url\": \"https://api.github.com/repos/ReactiveX/RxJava/issues/5140\",\n" +
                "    \"number\": 5140,\n" +
                "    \"state\": \"open\",\n" +
                "    \"locked\": false,\n" +
                "    \"title\": \"2.x: fix timed replay-like components replaying outdated items\",\n" +
                "    \"user\": {\n" +
                "      \"login\": \"akarnokd\",\n" +
                "      \"id\": 1269832,\n" +
                "      \"avatar_url\": \"https://avatars.githubusercontent.com/u/1269832?v=3\",\n" +
                "      \"gravatar_id\": \"\",\n" +
                "      \"url\": \"https://api.github.com/users/akarnokd\",\n" +
                "      \"html_url\": \"https://github.com/akarnokd\",\n" +
                "      \"followers_url\": \"https://api.github.com/users/akarnokd/followers\",\n" +
                "      \"following_url\": \"https://api.github.com/users/akarnokd/following{/other_user}\",\n" +
                "      \"gists_url\": \"https://api.github.com/users/akarnokd/gists{/gist_id}\",\n" +
                "      \"starred_url\": \"https://api.github.com/users/akarnokd/starred{/owner}{/repo}\",\n" +
                "      \"subscriptions_url\": \"https://api.github.com/users/akarnokd/subscriptions\",\n" +
                "      \"organizations_url\": \"https://api.github.com/users/akarnokd/orgs\",\n" +
                "      \"repos_url\": \"https://api.github.com/users/akarnokd/repos\",\n" +
                "      \"events_url\": \"https://api.github.com/users/akarnokd/events{/privacy}\",\n" +
                "      \"received_events_url\": \"https://api.github.com/users/akarnokd/received_events\",\n" +
                "      \"type\": \"User\",\n" +
                "      \"site_admin\": false\n" +
                "    },\n" +
                "    \"body\": \"The timed versions of `Flowable.replay()`, `ReplayProcessor`, `Observable.replay()` and `ReplaySubject` all replay outdated items to new subscribers and through the `getValues()` and `size()` state-peeking methods, similar to issue #3917 resolved via #4023.\\r\\n\\r\\nThe fix includes a node-walk for new subscribers that skips old entries. Some unit tests weren't logically considering the emission pattern (i.e., items timed out shouldn't appear) and have been fixed as well.\\r\\n\\r\\nReported in #5139.\",\n" +
                "    \"created_at\": \"2017-02-27T14:38:06Z\",\n" +
                "    \"updated_at\": \"2017-02-27T14:55:02Z\",\n" +
                "    \"closed_at\": null,\n" +
                "    \"merged_at\": null,\n" +
                "    \"merge_commit_sha\": \"d1ccedbbfbd250dd4ca9e373ce3a122614efae84\",\n" +
                "    \"assignee\": null,\n" +
                "    \"assignees\": [],\n" +
                "    \"milestone\": {\n" +
                "      \"url\": \"https://api.github.com/repos/ReactiveX/RxJava/milestones/21\",\n" +
                "      \"html_url\": \"https://github.com/ReactiveX/RxJava/milestone/21\",\n" +
                "      \"labels_url\": \"https://api.github.com/repos/ReactiveX/RxJava/milestones/21/labels\",\n" +
                "      \"id\": 2092132,\n" +
                "      \"number\": 21,\n" +
                "      \"title\": \"2.0 backlog\",\n" +
                "      \"description\": \"Things that weren't resolved until 2.0.0 GA (documentation, minor code changes).\",\n" +
                "      \"creator\": {\n" +
                "        \"login\": \"akarnokd\",\n" +
                "        \"id\": 1269832,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/1269832?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/akarnokd\",\n" +
                "        \"html_url\": \"https://github.com/akarnokd\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/akarnokd/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/akarnokd/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/akarnokd/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/akarnokd/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/akarnokd/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/akarnokd/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/akarnokd/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/akarnokd/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/akarnokd/received_events\",\n" +
                "        \"type\": \"User\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"open_issues\": 4,\n" +
                "      \"closed_issues\": 87,\n" +
                "      \"state\": \"open\",\n" +
                "      \"created_at\": \"2016-10-25T12:48:27Z\",\n" +
                "      \"updated_at\": \"2017-02-27T14:38:06Z\",\n" +
                "      \"due_on\": null,\n" +
                "      \"closed_at\": null\n" +
                "    },\n" +
                "    \"commits_url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/5140/commits\",\n" +
                "    \"review_comments_url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/5140/comments\",\n" +
                "    \"review_comment_url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/comments{/number}\",\n" +
                "    \"comments_url\": \"https://api.github.com/repos/ReactiveX/RxJava/issues/5140/comments\",\n" +
                "    \"statuses_url\": \"https://api.github.com/repos/ReactiveX/RxJava/statuses/6377b58f99ce270c71c0704c0b601c784d9ef6ed\",\n" +
                "    \"head\": {\n" +
                "      \"label\": \"akarnokd:ReplayNoOld\",\n" +
                "      \"ref\": \"ReplayNoOld\",\n" +
                "      \"sha\": \"6377b58f99ce270c71c0704c0b601c784d9ef6ed\",\n" +
                "      \"user\": {\n" +
                "        \"login\": \"akarnokd\",\n" +
                "        \"id\": 1269832,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/1269832?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/akarnokd\",\n" +
                "        \"html_url\": \"https://github.com/akarnokd\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/akarnokd/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/akarnokd/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/akarnokd/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/akarnokd/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/akarnokd/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/akarnokd/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/akarnokd/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/akarnokd/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/akarnokd/received_events\",\n" +
                "        \"type\": \"User\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"repo\": {\n" +
                "        \"id\": 14518033,\n" +
                "        \"name\": \"RxJava\",\n" +
                "        \"full_name\": \"akarnokd/RxJava\",\n" +
                "        \"owner\": {\n" +
                "          \"login\": \"akarnokd\",\n" +
                "          \"id\": 1269832,\n" +
                "          \"avatar_url\": \"https://avatars.githubusercontent.com/u/1269832?v=3\",\n" +
                "          \"gravatar_id\": \"\",\n" +
                "          \"url\": \"https://api.github.com/users/akarnokd\",\n" +
                "          \"html_url\": \"https://github.com/akarnokd\",\n" +
                "          \"followers_url\": \"https://api.github.com/users/akarnokd/followers\",\n" +
                "          \"following_url\": \"https://api.github.com/users/akarnokd/following{/other_user}\",\n" +
                "          \"gists_url\": \"https://api.github.com/users/akarnokd/gists{/gist_id}\",\n" +
                "          \"starred_url\": \"https://api.github.com/users/akarnokd/starred{/owner}{/repo}\",\n" +
                "          \"subscriptions_url\": \"https://api.github.com/users/akarnokd/subscriptions\",\n" +
                "          \"organizations_url\": \"https://api.github.com/users/akarnokd/orgs\",\n" +
                "          \"repos_url\": \"https://api.github.com/users/akarnokd/repos\",\n" +
                "          \"events_url\": \"https://api.github.com/users/akarnokd/events{/privacy}\",\n" +
                "          \"received_events_url\": \"https://api.github.com/users/akarnokd/received_events\",\n" +
                "          \"type\": \"User\",\n" +
                "          \"site_admin\": false\n" +
                "        },\n" +
                "        \"private\": false,\n" +
                "        \"html_url\": \"https://github.com/akarnokd/RxJava\",\n" +
                "        \"description\": \"RxJava - a library for composing asynchronous and event-based programs using observable sequences for the Java VM.\",\n" +
                "        \"fork\": true,\n" +
                "        \"url\": \"https://api.github.com/repos/akarnokd/RxJava\",\n" +
                "        \"forks_url\": \"https://api.github.com/repos/akarnokd/RxJava/forks\",\n" +
                "        \"keys_url\": \"https://api.github.com/repos/akarnokd/RxJava/keys{/key_id}\",\n" +
                "        \"collaborators_url\": \"https://api.github.com/repos/akarnokd/RxJava/collaborators{/collaborator}\",\n" +
                "        \"teams_url\": \"https://api.github.com/repos/akarnokd/RxJava/teams\",\n" +
                "        \"hooks_url\": \"https://api.github.com/repos/akarnokd/RxJava/hooks\",\n" +
                "        \"issue_events_url\": \"https://api.github.com/repos/akarnokd/RxJava/issues/events{/number}\",\n" +
                "        \"events_url\": \"https://api.github.com/repos/akarnokd/RxJava/events\",\n" +
                "        \"assignees_url\": \"https://api.github.com/repos/akarnokd/RxJava/assignees{/user}\",\n" +
                "        \"branches_url\": \"https://api.github.com/repos/akarnokd/RxJava/branches{/branch}\",\n" +
                "        \"tags_url\": \"https://api.github.com/repos/akarnokd/RxJava/tags\",\n" +
                "        \"blobs_url\": \"https://api.github.com/repos/akarnokd/RxJava/git/blobs{/sha}\",\n" +
                "        \"git_tags_url\": \"https://api.github.com/repos/akarnokd/RxJava/git/tags{/sha}\",\n" +
                "        \"git_refs_url\": \"https://api.github.com/repos/akarnokd/RxJava/git/refs{/sha}\",\n" +
                "        \"trees_url\": \"https://api.github.com/repos/akarnokd/RxJava/git/trees{/sha}\",\n" +
                "        \"statuses_url\": \"https://api.github.com/repos/akarnokd/RxJava/statuses/{sha}\",\n" +
                "        \"languages_url\": \"https://api.github.com/repos/akarnokd/RxJava/languages\",\n" +
                "        \"stargazers_url\": \"https://api.github.com/repos/akarnokd/RxJava/stargazers\",\n" +
                "        \"contributors_url\": \"https://api.github.com/repos/akarnokd/RxJava/contributors\",\n" +
                "        \"subscribers_url\": \"https://api.github.com/repos/akarnokd/RxJava/subscribers\",\n" +
                "        \"subscription_url\": \"https://api.github.com/repos/akarnokd/RxJava/subscription\",\n" +
                "        \"commits_url\": \"https://api.github.com/repos/akarnokd/RxJava/commits{/sha}\",\n" +
                "        \"git_commits_url\": \"https://api.github.com/repos/akarnokd/RxJava/git/commits{/sha}\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/akarnokd/RxJava/comments{/number}\",\n" +
                "        \"issue_comment_url\": \"https://api.github.com/repos/akarnokd/RxJava/issues/comments{/number}\",\n" +
                "        \"contents_url\": \"https://api.github.com/repos/akarnokd/RxJava/contents/{+path}\",\n" +
                "        \"compare_url\": \"https://api.github.com/repos/akarnokd/RxJava/compare/{base}...{head}\",\n" +
                "        \"merges_url\": \"https://api.github.com/repos/akarnokd/RxJava/merges\",\n" +
                "        \"archive_url\": \"https://api.github.com/repos/akarnokd/RxJava/{archive_format}{/ref}\",\n" +
                "        \"downloads_url\": \"https://api.github.com/repos/akarnokd/RxJava/downloads\",\n" +
                "        \"issues_url\": \"https://api.github.com/repos/akarnokd/RxJava/issues{/number}\",\n" +
                "        \"pulls_url\": \"https://api.github.com/repos/akarnokd/RxJava/pulls{/number}\",\n" +
                "        \"milestones_url\": \"https://api.github.com/repos/akarnokd/RxJava/milestones{/number}\",\n" +
                "        \"notifications_url\": \"https://api.github.com/repos/akarnokd/RxJava/notifications{?since,all,participating}\",\n" +
                "        \"labels_url\": \"https://api.github.com/repos/akarnokd/RxJava/labels{/name}\",\n" +
                "        \"releases_url\": \"https://api.github.com/repos/akarnokd/RxJava/releases{/id}\",\n" +
                "        \"deployments_url\": \"https://api.github.com/repos/akarnokd/RxJava/deployments\",\n" +
                "        \"created_at\": \"2013-11-19T08:04:52Z\",\n" +
                "        \"updated_at\": \"2016-11-13T20:47:29Z\",\n" +
                "        \"pushed_at\": \"2017-02-27T16:45:24Z\",\n" +
                "        \"git_url\": \"git://github.com/akarnokd/RxJava.git\",\n" +
                "        \"ssh_url\": \"git@github.com:akarnokd/RxJava.git\",\n" +
                "        \"clone_url\": \"https://github.com/akarnokd/RxJava.git\",\n" +
                "        \"svn_url\": \"https://github.com/akarnokd/RxJava\",\n" +
                "        \"homepage\": \"\",\n" +
                "        \"size\": 31304,\n" +
                "        \"stargazers_count\": 2,\n" +
                "        \"watchers_count\": 2,\n" +
                "        \"language\": \"Java\",\n" +
                "        \"has_issues\": false,\n" +
                "        \"has_downloads\": true,\n" +
                "        \"has_wiki\": true,\n" +
                "        \"has_pages\": false,\n" +
                "        \"forks_count\": 1,\n" +
                "        \"mirror_url\": null,\n" +
                "        \"open_issues_count\": 0,\n" +
                "        \"forks\": 1,\n" +
                "        \"open_issues\": 0,\n" +
                "        \"watchers\": 2,\n" +
                "        \"default_branch\": \"2.x\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"base\": {\n" +
                "      \"label\": \"ReactiveX:2.x\",\n" +
                "      \"ref\": \"2.x\",\n" +
                "      \"sha\": \"a03bf90c19e0eed80c677eb0e3071079220fd392\",\n" +
                "      \"user\": {\n" +
                "        \"login\": \"ReactiveX\",\n" +
                "        \"id\": 6407041,\n" +
                "        \"avatar_url\": \"https://avatars.githubusercontent.com/u/6407041?v=3\",\n" +
                "        \"gravatar_id\": \"\",\n" +
                "        \"url\": \"https://api.github.com/users/ReactiveX\",\n" +
                "        \"html_url\": \"https://github.com/ReactiveX\",\n" +
                "        \"followers_url\": \"https://api.github.com/users/ReactiveX/followers\",\n" +
                "        \"following_url\": \"https://api.github.com/users/ReactiveX/following{/other_user}\",\n" +
                "        \"gists_url\": \"https://api.github.com/users/ReactiveX/gists{/gist_id}\",\n" +
                "        \"starred_url\": \"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}\",\n" +
                "        \"subscriptions_url\": \"https://api.github.com/users/ReactiveX/subscriptions\",\n" +
                "        \"organizations_url\": \"https://api.github.com/users/ReactiveX/orgs\",\n" +
                "        \"repos_url\": \"https://api.github.com/users/ReactiveX/repos\",\n" +
                "        \"events_url\": \"https://api.github.com/users/ReactiveX/events{/privacy}\",\n" +
                "        \"received_events_url\": \"https://api.github.com/users/ReactiveX/received_events\",\n" +
                "        \"type\": \"Organization\",\n" +
                "        \"site_admin\": false\n" +
                "      },\n" +
                "      \"repo\": {\n" +
                "        \"id\": 7508411,\n" +
                "        \"name\": \"RxJava\",\n" +
                "        \"full_name\": \"ReactiveX/RxJava\",\n" +
                "        \"owner\": {\n" +
                "          \"login\": \"ReactiveX\",\n" +
                "          \"id\": 6407041,\n" +
                "          \"avatar_url\": \"https://avatars.githubusercontent.com/u/6407041?v=3\",\n" +
                "          \"gravatar_id\": \"\",\n" +
                "          \"url\": \"https://api.github.com/users/ReactiveX\",\n" +
                "          \"html_url\": \"https://github.com/ReactiveX\",\n" +
                "          \"followers_url\": \"https://api.github.com/users/ReactiveX/followers\",\n" +
                "          \"following_url\": \"https://api.github.com/users/ReactiveX/following{/other_user}\",\n" +
                "          \"gists_url\": \"https://api.github.com/users/ReactiveX/gists{/gist_id}\",\n" +
                "          \"starred_url\": \"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}\",\n" +
                "          \"subscriptions_url\": \"https://api.github.com/users/ReactiveX/subscriptions\",\n" +
                "          \"organizations_url\": \"https://api.github.com/users/ReactiveX/orgs\",\n" +
                "          \"repos_url\": \"https://api.github.com/users/ReactiveX/repos\",\n" +
                "          \"events_url\": \"https://api.github.com/users/ReactiveX/events{/privacy}\",\n" +
                "          \"received_events_url\": \"https://api.github.com/users/ReactiveX/received_events\",\n" +
                "          \"type\": \"Organization\",\n" +
                "          \"site_admin\": false\n" +
                "        },\n" +
                "        \"private\": false,\n" +
                "        \"html_url\": \"https://github.com/ReactiveX/RxJava\",\n" +
                "        \"description\": \"RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.\",\n" +
                "        \"fork\": false,\n" +
                "        \"url\": \"https://api.github.com/repos/ReactiveX/RxJava\",\n" +
                "        \"forks_url\": \"https://api.github.com/repos/ReactiveX/RxJava/forks\",\n" +
                "        \"keys_url\": \"https://api.github.com/repos/ReactiveX/RxJava/keys{/key_id}\",\n" +
                "        \"collaborators_url\": \"https://api.github.com/repos/ReactiveX/RxJava/collaborators{/collaborator}\",\n" +
                "        \"teams_url\": \"https://api.github.com/repos/ReactiveX/RxJava/teams\",\n" +
                "        \"hooks_url\": \"https://api.github.com/repos/ReactiveX/RxJava/hooks\",\n" +
                "        \"issue_events_url\": \"https://api.github.com/repos/ReactiveX/RxJava/issues/events{/number}\",\n" +
                "        \"events_url\": \"https://api.github.com/repos/ReactiveX/RxJava/events\",\n" +
                "        \"assignees_url\": \"https://api.github.com/repos/ReactiveX/RxJava/assignees{/user}\",\n" +
                "        \"branches_url\": \"https://api.github.com/repos/ReactiveX/RxJava/branches{/branch}\",\n" +
                "        \"tags_url\": \"https://api.github.com/repos/ReactiveX/RxJava/tags\",\n" +
                "        \"blobs_url\": \"https://api.github.com/repos/ReactiveX/RxJava/git/blobs{/sha}\",\n" +
                "        \"git_tags_url\": \"https://api.github.com/repos/ReactiveX/RxJava/git/tags{/sha}\",\n" +
                "        \"git_refs_url\": \"https://api.github.com/repos/ReactiveX/RxJava/git/refs{/sha}\",\n" +
                "        \"trees_url\": \"https://api.github.com/repos/ReactiveX/RxJava/git/trees{/sha}\",\n" +
                "        \"statuses_url\": \"https://api.github.com/repos/ReactiveX/RxJava/statuses/{sha}\",\n" +
                "        \"languages_url\": \"https://api.github.com/repos/ReactiveX/RxJava/languages\",\n" +
                "        \"stargazers_url\": \"https://api.github.com/repos/ReactiveX/RxJava/stargazers\",\n" +
                "        \"contributors_url\": \"https://api.github.com/repos/ReactiveX/RxJava/contributors\",\n" +
                "        \"subscribers_url\": \"https://api.github.com/repos/ReactiveX/RxJava/subscribers\",\n" +
                "        \"subscription_url\": \"https://api.github.com/repos/ReactiveX/RxJava/subscription\",\n" +
                "        \"commits_url\": \"https://api.github.com/repos/ReactiveX/RxJava/commits{/sha}\",\n" +
                "        \"git_commits_url\": \"https://api.github.com/repos/ReactiveX/RxJava/git/commits{/sha}\",\n" +
                "        \"comments_url\": \"https://api.github.com/repos/ReactiveX/RxJava/comments{/number}\",\n" +
                "        \"issue_comment_url\": \"https://api.github.com/repos/ReactiveX/RxJava/issues/comments{/number}\",\n" +
                "        \"contents_url\": \"https://api.github.com/repos/ReactiveX/RxJava/contents/{+path}\",\n" +
                "        \"compare_url\": \"https://api.github.com/repos/ReactiveX/RxJava/compare/{base}...{head}\",\n" +
                "        \"merges_url\": \"https://api.github.com/repos/ReactiveX/RxJava/merges\",\n" +
                "        \"archive_url\": \"https://api.github.com/repos/ReactiveX/RxJava/{archive_format}{/ref}\",\n" +
                "        \"downloads_url\": \"https://api.github.com/repos/ReactiveX/RxJava/downloads\",\n" +
                "        \"issues_url\": \"https://api.github.com/repos/ReactiveX/RxJava/issues{/number}\",\n" +
                "        \"pulls_url\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}\",\n" +
                "        \"milestones_url\": \"https://api.github.com/repos/ReactiveX/RxJava/milestones{/number}\",\n" +
                "        \"notifications_url\": \"https://api.github.com/repos/ReactiveX/RxJava/notifications{?since,all,participating}\",\n" +
                "        \"labels_url\": \"https://api.github.com/repos/ReactiveX/RxJava/labels{/name}\",\n" +
                "        \"releases_url\": \"https://api.github.com/repos/ReactiveX/RxJava/releases{/id}\",\n" +
                "        \"deployments_url\": \"https://api.github.com/repos/ReactiveX/RxJava/deployments\",\n" +
                "        \"created_at\": \"2013-01-08T20:11:48Z\",\n" +
                "        \"updated_at\": \"2017-02-27T16:42:12Z\",\n" +
                "        \"pushed_at\": \"2017-02-27T16:45:21Z\",\n" +
                "        \"git_url\": \"git://github.com/ReactiveX/RxJava.git\",\n" +
                "        \"ssh_url\": \"git@github.com:ReactiveX/RxJava.git\",\n" +
                "        \"clone_url\": \"https://github.com/ReactiveX/RxJava.git\",\n" +
                "        \"svn_url\": \"https://github.com/ReactiveX/RxJava\",\n" +
                "        \"homepage\": \"\",\n" +
                "        \"size\": 38221,\n" +
                "        \"stargazers_count\": 21789,\n" +
                "        \"watchers_count\": 21789,\n" +
                "        \"language\": \"Java\",\n" +
                "        \"has_issues\": true,\n" +
                "        \"has_downloads\": true,\n" +
                "        \"has_wiki\": true,\n" +
                "        \"has_pages\": true,\n" +
                "        \"forks_count\": 3820,\n" +
                "        \"mirror_url\": null,\n" +
                "        \"open_issues_count\": 29,\n" +
                "        \"forks\": 3820,\n" +
                "        \"open_issues\": 29,\n" +
                "        \"watchers\": 21789,\n" +
                "        \"default_branch\": \"2.x\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"_links\": {\n" +
                "      \"self\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/5140\"\n" +
                "      },\n" +
                "      \"html\": {\n" +
                "        \"href\": \"https://github.com/ReactiveX/RxJava/pull/5140\"\n" +
                "      },\n" +
                "      \"issue\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/issues/5140\"\n" +
                "      },\n" +
                "      \"comments\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/issues/5140/comments\"\n" +
                "      },\n" +
                "      \"review_comments\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/5140/comments\"\n" +
                "      },\n" +
                "      \"review_comment\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/comments{/number}\"\n" +
                "      },\n" +
                "      \"commits\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/pulls/5140/commits\"\n" +
                "      },\n" +
                "      \"statuses\": {\n" +
                "        \"href\": \"https://api.github.com/repos/ReactiveX/RxJava/statuses/6377b58f99ce270c71c0704c0b601c784d9ef6ed\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "]"
    }
}